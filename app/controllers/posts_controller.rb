class PostsController < ApplicationController
  before_action :ensure_editor, only: [:edit, :update, :destroy]
  
  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.new(post_params)
    @post.sub_id = params[:sub_id]
    
    if @post.save
      redirect_to post_url(@post)
    else
      flash.now[:errors] = @post.errors.full_messages
      render :new
    end
    
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      redirect_to post_url(@post)
    else
      flash[:errors] = @post.errors.full_messages
      render :edit
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @sub = @post.sub
    @post.destroy
    
    redirect_to sub_url(@sub)
  end

  def show
    @post = Post.find(params[:id])
    @all_comments = hashify_comments(@post.comments)
  end
  
  
  private
  
  def post_params
    params.require(:post).permit(:title, :url, :content)
  end
  
  
  def hashify_comments(comments)
    h = {}
    
    comments.each do |comment|
      h[comment.parent_comment_id] ||= []
      h[comment.parent_comment_id] << comment
    end
    
    h
    
  end
end
