class SubsController < ApplicationController
  before_action :ensure_moderator, only: [:edit, :update, :destroy]
  
  
  def edit
    @sub = Sub.find(params[:id])
  end

  def create
    @sub = current_user.subs.new(sub_params)
    
    if @sub.save
      redirect_to sub_url(@sub)
    else
      flash.now[:errors] = @sub.errors.full_messages
      render :new
    end
  end

  def new
    @sub = Sub.new
  end

  def destroy
    Sub.find(params[:id]).destroy
    
    redirect_to subs_url
  end

  def update
    @sub = Sub.find(params[:id])
    
    if @sub.update_attributes(sub_params)
      redirect_to sub_url(@sub)
    else
      flash.now[:errors] = @sub.errors.full_messages
      render :edit
    end
    
  end

  def index
    @subs = Sub.all
  end
  
  def show
    @sub = Sub.find(params[:id])
    @post = Post.new
  end
  
  
  
  private
  
  def sub_params
    params.require(:sub).permit(:title, :description)
  end
  
  
  def ensure_moderator
    @sub = Sub.find(params[:id])
    redirect_to sub_url(@sub) unless moderator?
  end
  
  
end
