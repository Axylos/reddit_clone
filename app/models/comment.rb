class Comment < ActiveRecord::Base
  validates :content, :submitter, :post_id, presence: true
  
  belongs_to :post
  belongs_to :submitter, class_name: "User", foreign_key: :submitter_id
  
  has_many :child_comments, class_name: "Comment", foreign_key: :parent_comment_id
  
  belongs_to :parent_comment, class_name: "Comment", foreign_key: :parent_comment_id
  
  
  
end
