class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  helper_method :signed_in?, :current_user, :moderator?, :editor?
  
  def current_user
    return nil unless session[:token]
    @current_user ||= User.find_by(session_token: session[:token])
  end
  
  def sign_in(user)
    @current_user = user
    session[:token] = user.reset_session_token!
  end
  
  def sign_out
    @current_user = nil
    current_user.reset_session_token!
    session[:token] = nil 
  end
  
  def signed_in?
    !!current_user
  end
  
  def moderator?
    @sub = Sub.find(params[:id])
    current_user == @sub.moderator
  end
  
  def ensure_editor
    @post = Post.find(params[:id])
    redirect_to post_url(@post) unless editor?
  end
  
  def editor?
    @post = Post.find(params[:id])
    current_user == @post.submitter || moderator?
  end
  
end
