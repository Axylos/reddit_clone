class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :value
      t.references: :value, polymorphic: true

      t.timestamps
    end
  end
end
