class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :content, null: false
      t.integer :submitter, null: false, index: true
      t.integer :post_id, null: false, index: true
      t.integer :parent_comment_id, index: true

      t.timestamps
    end
  end
end
