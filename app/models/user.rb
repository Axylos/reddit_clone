class User < ActiveRecord::Base
  before_validation :ensure_session_token
  validates :username, :password_digest, presence: true 
  validates :password, length: { minimum: 6, allow_nil: true }
  
  has_many :subs, class_name: "Sub", foreign_key: :moderator_id
  
  has_many :posts, class_name: "Post", foreign_key: :submitter_id
  
  has_many :comments, class_name: "Comment", foreign_key: :submitter_id
  
  
  attr_reader :password
  
  
  def self.generate_session_token
    SecureRandom.urlsafe_base64(16)
  end
  
  
  def reset_session_token!
    self.session_token = User.generate_session_token
    self.save!
    self.session_token
  end
  
  def password=(unencrypted_password)
    @password = unencrypted_password
    self.password_digest = BCrypt::Password.create(unencrypted_password)
    
  end
  
  
  def is_password?(unencrypted_password)
    BCrypt::Password.new(self.password_digest)
                    .is_password?(unencrypted_password)
    
  end
  
  
  def ensure_session_token
    self.session_token ||= User.generate_session_token
  end

  
  
end
