RedditClone::Application.routes.draw do
  
  resources :users, only: [:new, :index, :create, :destroy]
  
  resource :session, only: [:create, :destroy, :new]
  
  shallow do 
    resources :subs do 
      resources :posts do
        resources :comments, only: [:new, :create, :edit, :update, :destroy] do
          resources :comments, only: :new
        end
      end
    end
  end
  

  
  
  
end
