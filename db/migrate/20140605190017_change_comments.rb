class ChangeComments < ActiveRecord::Migration
  def change
    rename_column :comments, :submitter, :submitter_id
  end
end
