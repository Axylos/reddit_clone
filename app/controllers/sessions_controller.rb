class SessionsController < ApplicationController
  def create
    @user = User.find_by(username: params[:user][:username] )
    if @user && @user.is_password?(params[:user][:password])
      sign_in(@user)
      redirect_to users_url
    else
      flash.now[:errors] = ["Invalid credentials"]
      render :new
    end
  end

  def destroy
    sign_out
    redirect_to new_session_url
  end
  
  def new
    @user = User.new
  end
  
  
  
  
end
