class Post < ActiveRecord::Base
  belongs_to :sub, class_name: "Sub", foreign_key: :sub_id
  belongs_to :submitter, class_name: "User", foreign_key: :submitter_id
  
  validates :title, :sub_id, :submitter_id, presence: true
  validates :title, uniqueness: { scope: :sub_id }
  
  has_many :comments
  
  
  has_many( :top_level_comments,
            -> { where parent_comment_id: nil},
            class_name: "Comment",
            foreign_key: :post_id
          )
end
