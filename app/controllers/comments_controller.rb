class CommentsController < ApplicationController
   
  before_action :ensure_editor, only: [:edit, :update, :destroy]
 
  def new
    
    if params.keys.include? "comment_id"
      @comment = Comment.find(params[:comment_id]).child_comments.new
      @post = @comment.parent_comment.post
      
      
    else
      @post = Post.find(params[:post_id])
      @comment = Comment.new
    end
      
  end

  def create
    @comment = Comment.new(comment_params)
    @comment.submitter = current_user
    
    if @comment.save
      redirect_to post_url(@comment.post)
    else
      flash.now[:errors] = @comment.errors.full_messages
      render :new
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @post = @comment.post
    @comment.destroy
    
    redirect_to post_url(@post)
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def update
    @comment = Comment.find(params[:id])
    if @comment.update_attributes(comment_params)
      redirect_to post_url(@comment.post)
    else
      flash.now[:errors] = @comment.errors.full_messages
      render :edit
    end
  end
  
  
  private
  
  def comment_params
    params.require(:comment).permit(:content, :post_id, :parent_comment_id)
  end
end
